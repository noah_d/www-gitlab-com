---
layout: markdown_page
title: Product Direction - Fulfillment - Self-Service Purchase
description: "The Fulfillment team at GitLab focuses on creating and supporting the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
Last reviewed: 2022-08
